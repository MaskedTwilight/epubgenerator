﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;

namespace EPubGenerator
{
    public class FolderTreeViewGenerator
    {
        private string path;

        public class FolderInfo : DependencyObject
        {
            public string Name { get; set; }
            public string Path { get; set; }
            public FileInfo[] FileInfo { get; set; }
        }

        public static readonly DependencyProperty FolderInfoProperty = DependencyProperty.Register(
            "FolderInfo",
            typeof(object),
            typeof(FolderInfo),
            new PropertyMetadata(null)
            );

        public FolderTreeViewGenerator(string path)
        {
            this.path = path;
        }

        public TreeViewItem GenerateNode()
        {
            var rootDirectoryInfo = new DirectoryInfo(this.path);
            return CreateDirectoryNode(rootDirectoryInfo);
        }

        private TreeViewItem CreateDirectoryNode(DirectoryInfo directoryInfo)
        {
            var directoryNode = new TreeViewItem();
            directoryNode.Header = directoryInfo.Name;
            var folderInfo = new FolderInfo
            {
                Name = directoryInfo.Name,
                Path = directoryInfo.FullName,
                FileInfo = directoryInfo.GetFiles(),
        };

            directoryNode.SetValue(FolderInfoProperty, folderInfo);

            foreach (var directory in directoryInfo.GetDirectories())
            {
                directoryNode.Items.Add(CreateDirectoryNode(directory));
            }

            return directoryNode;
        }
    }
}
