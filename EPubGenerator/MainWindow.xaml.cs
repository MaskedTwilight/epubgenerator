﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Ookii.Dialogs.Wpf;

namespace EPubGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnGenerate_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnPickFolder_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new VistaFolderBrowserDialog();
            if (dialog.ShowDialog(this).GetValueOrDefault())
            {
                SelectedFolder.Content = dialog.SelectedPath;
                var folderTreeGenerator = new FolderTreeViewGenerator(dialog.SelectedPath);
                var rootNode = folderTreeGenerator.GenerateNode();
                rootNode.IsSelected = true;
                FolderTree.Items.Clear();
                FolderTree.Items.Add(rootNode);
            }
        }

        private void btnStartGenerating_Click(object sender, RoutedEventArgs e)
        {
            var currentNode = FolderTree.ItemContainerGenerator.ContainerFromItem(FolderTree.SelectedItem);
            var folderInfo = currentNode.GetValue(FolderTreeViewGenerator.FolderInfoProperty) as FolderTreeViewGenerator.FolderInfo;
            var imageGenerator = new ImageViewerGenerator(folderInfo.FileInfo);
            var images = imageGenerator.GenerateImages();

            foreach(var image in images)
            {
                ImageList.Children.Add(image);
            }
        }
    }
}
