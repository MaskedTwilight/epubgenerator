﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace EPubGenerator
{
    class ImageViewerGenerator
    {
        private FileInfo[] fileInfoList;

        private HashSet<string> AcceptableFormats = new HashSet<string>()
        {
            ".png", ".apng", ".avif", ".gif", ".jpg", ".jpeg", ".jfif", ".pjpeg", ".pjp", ".svg", ".webp"
        };

        public ImageViewerGenerator(FileInfo[] fileInfoList)
        {
            this.fileInfoList = fileInfoList;
        }



        public List<Image> GenerateImages()
        {
            var imageList = new List<Image>();
            foreach(var file in this.fileInfoList)
            {
                if (!AcceptableFormats.Contains(file.Extension))
                {
                    continue;
                }

                var image = new Image();
                var bitmapImage = new BitmapImage(new Uri(file.FullName, UriKind.Absolute));
                image.Source = bitmapImage;
                imageList.Add(image);
            }

            return imageList;
        }
    }
}
